<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ATwiIkFkMOSo5SKF5shO7tqpAE0lEYUiQwG9ByfaaR0BDyhHVdK1y8o7BFqBqz9ICBDqTRfTasdDzuW3ETxLfA==');
define('SECURE_AUTH_KEY',  'R8Y9V+5QyeC7/45GMhuztAufims4LWYVouiSMv6AHWlQWGrV5lTjBdIbURTBiCGZVrZZesiGjjcAB/odjbdnHg==');
define('LOGGED_IN_KEY',    'Xb8PSwkJ3JA2WZnwCzgL5vwH6mQ46m0tv4cve4WJPecEOcv8DsZs9NIiR90ClaUXmUS2M0ijmaDXSE5sHkGTyg==');
define('NONCE_KEY',        'HTdHM3GFBBbCDrbNqIzePNIjtyvMSZRzu5eP2/K+HsCeT5vTnfekacYyz7ovNWvde7v0VSKyqew+Lt7W68h2aw==');
define('AUTH_SALT',        '8H45xP/o8nLXThctdf1IOBGpn1ZVZtu5ni/GZJUWqzSGoWMIqx3bfhgpKJ3OJoXqd+iNiI0g2D/HAfCUPO8YGg==');
define('SECURE_AUTH_SALT', '/RNXkG3lR2Ez4pNJzMhNyQUwPDD5SpTlA3r6x+OZST9eycjaJrKa+g5BaG+a3RPV9WXtoAiMmD1M4lMPRaepGg==');
define('LOGGED_IN_SALT',   '8kml0MzJBPwGUfGvlYAQ5RO3iLL0x+abfFfvCmOe/eNWk+eQ/Fb6vSRmWDlkfxGXNDtKtOsZrhlyeyxEleJRjg==');
define('NONCE_SALT',       'Dc+d6gjN+7N49vel9RQYuJ/IZOl03n5lZ88Ipb234HZRYZHzFDmvfYOqJbWUejWPVDs9JJr2DgcWiRQgG6h0Wg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
